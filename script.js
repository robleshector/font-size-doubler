// Set Elements
const increase = document.querySelector("#increase");
const letter = document.querySelector("#letter");


// Set Variables
let fontSize = 16;


// Functions
const updateFontSize = function() {
    letter.style.fontSize = `${fontSize}px`
}

const increaseFontSize = function() {
    increase.setAttribute("disabled", "")
    fontSize *= 2;
    updateFontSize();
    setTimeout(function() {
        increase.removeAttribute("disabled")
    }, 1000)
}


// Set Listener
increase.addEventListener("click", increaseFontSize, false);


// Initialize
updateFontSize();